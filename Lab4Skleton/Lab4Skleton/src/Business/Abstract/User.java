/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Abstract;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
/**
 *
 * @author AEDSpring2019
 */
public abstract class User {
    private String password;
    private String userName;
    private String role;
    Date d;
    
    public Date getD1(){
    return d;
    }
    
    public void setD1(Date d2){
        this.d = d2;
    }
    public User(String password, String userName, String role) {
        this.password = password;
        this.userName = userName;
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    abstract public boolean verify(String password);

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
DateFormat df = DateFormat.getDateInstance();
return df.format(getD1());    }
    
}
