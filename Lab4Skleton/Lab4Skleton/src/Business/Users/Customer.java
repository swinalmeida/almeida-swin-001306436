/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import java.util.Date;
import Business.ProductDirectory;
import java.text.DateFormat;

/**
 *
 * @author AEDSpring2019
 */
public class Customer extends User implements Comparable<Supplier> {
    private ProductDirectory directory;
    Date d = new Date();
    
    public Date getD1(){
        return d;
    
    }
    
    public void setD1(Date d1)
    {
        this.d=d1;
    }
    
    public Customer(String password, String userName)
    {
        super(password, userName, "CUSTOMER");
        directory = new ProductDirectory();
    }
    public ProductDirectory getDirectory(){
        return directory;
    }
    public void setDirectory(ProductDirectory directory)
    {
        this.directory = directory; 
    }
    
    @Override
    public int compareTo(Supplier o)
    {
        return o.getUserName().compareTo(this.getUserName());
    }
    
    public String toDateString()
    {
        DateFormat df = DateFormat.getDateTimeInstance();
        return df.format(getD1());
    }
    
    public String toString()
    {
        return getUserName();
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
}
