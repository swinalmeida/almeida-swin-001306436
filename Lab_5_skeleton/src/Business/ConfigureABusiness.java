/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Organization.AdminOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabOrganization;
import Business.UserAccount.UserAccount;

/**
 *
 * @author ran
 */
public class ConfigureABusiness {
    
    public static Business configure(){
        // Three roles: LabAssistant, Doctor, Admin
        
        Business business = Business.getInstance();
        AdminOrganization adminOrganization = new AdminOrganization();

        business.getOrganizationDirectory().getOrganizationList().add(adminOrganization);

        Employee employee = new Employee();
        employee.setName("Abc xyz");

        UserAccount account = new UserAccount();
        account.setUsername("admin");
        account.setPassword("admin");
        account.setRole("Admin");

        account.setEmployee(employee);

        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee);
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(account);

        DoctorOrganization doctorOrganization = new DoctorOrganization();

        business.getOrganizationDirectory().getOrganizationList().add(doctorOrganization);

        Employee employee2 = new Employee();
        employee2.setName("Jacky");

        UserAccount account2 = new UserAccount();
        account2.setUsername("doctor");
        account2.setPassword("doctor");
        account2.setRole("Doctor");

        account2.setEmployee(employee2);

        doctorOrganization.getEmployeeDirectory().getEmployeeList().add(employee2);
        doctorOrganization.getUserAccountDirectory().getUserAccountList().add(account2);

        LabOrganization labOrganization = new LabOrganization();

        business.getOrganizationDirectory().getOrganizationList().add(labOrganization);

        Employee employee3 = new Employee();
        employee3.setName("Ben");

        UserAccount account3 = new UserAccount();
        account3.setUsername("lab");
        account3.setPassword("lab");
        account3.setRole("LabAssistant");

        account3.setEmployee(employee3);

        labOrganization.getEmployeeDirectory().getEmployeeList().add(employee3);
        labOrganization.getUserAccountDirectory().getUserAccountList().add(account3);

        return business;

    }
    
}
