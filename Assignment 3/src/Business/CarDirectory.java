/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
import java.util.Date;



/**
 *
 * @author almei
 */
public class CarDirectory {
        

public ArrayList<Car> carDirectory;
  
public Date LastUpdated;
public void setLastUpdated(Date LastUpdated)
{
    this.LastUpdated = LastUpdated;
}

public Date getLastUpdated(){
    return LastUpdated;
}



public CarDirectory(){
this.carDirectory = new ArrayList<>();

        
        Car car1 = new Car(true, "Ferrari", 2019, 1, 2, 1,"Red One", "Boston", true);
        Car car2 = new Car(true, "BMW", 2019, 1, 4, 2, "X1", "New York", true);
        Car car3 = new Car(true, "Toyota", 2018, 1, 4, 3, "T1", "Boston", true);
        Car car4 = new Car(true, "GM", 2018, 1, 4, 4, "G1", "New York", true);
        Car car5 = new Car(true, "Toyota", 2017, 1, 4, 5, "T2", "Chicago", true);
        Car car6 = new Car(true, "GM", 2017, 1, 4, 6, "G2", "Chicago", true);
        Car car7 = new Car(true, "Ferrari", 2016, 1, 4, 7, "", "Seattle", true);
        Car car8 = new Car(true, "BMW", 2016, 1, 4, 8, "X2", "Seattle", true);
        Car car9 = new Car(true, "Toyota", 2019, 1, 4, 9, "T3", "Austin", true);
        Car car10 = new Car(true, "GM", 2019, 1, 4, 10, "G3", "Austin", true);
        
        carDirectory.add(car1);
        carDirectory.add(car2);
        carDirectory.add(car3);
        carDirectory.add(car4);
        carDirectory.add(car5);
        carDirectory.add(car6);
        carDirectory.add(car7);
        carDirectory.add(car8);
        carDirectory.add(car9);
        carDirectory.add(car10);
        
    }

    public ArrayList<Car> getCarDirectory() {
        return carDirectory;
    }

    public void setCarDirectory(ArrayList<Car> carDirectory) {
        this.carDirectory = carDirectory;
    }
    
   public Car addProduct(){
        Car newProduct = new Car();
        carDirectory.add(newProduct);
        return newProduct;
    }
    
    public void deleteProduct(Car product){
        carDirectory.remove(product);
    }

   
   
    
    
}
