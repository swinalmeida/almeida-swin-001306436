/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Interface;
import Business.CarDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author almei
 */
public class FleetManagerPanel extends javax.swing.JPanel {

    /** Creates new form FleetManagerPanel */
    private CarDirectory carDir;
    private JPanel userProcessContainer;
    
    FleetManagerPanel(JPanel userProcessContainer, CarDirectory carDir) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.carDir = carDir;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnfleetCatalog = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();

        btnfleetCatalog.setText("Fleet Catalog");
        btnfleetCatalog.setPreferredSize(new java.awt.Dimension(600, 30));
        btnfleetCatalog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnfleetCatalogActionPerformed(evt);
            }
        });

        btnSearch.setText("Manage Fleet");
        btnSearch.setPreferredSize(new java.awt.Dimension(600, 30));
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(121, 121, 121)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnfleetCatalog, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(179, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(btnfleetCatalog, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(447, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnfleetCatalogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnfleetCatalogActionPerformed
        // TODO add your handling code here:
        FleetCatalogJPanel panel = new FleetCatalogJPanel(userProcessContainer, carDir);
        userProcessContainer.add("FleetCatalogJPanel",panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnfleetCatalogActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        // TODO add your handling code here:
        SearchJPanel panel = new SearchJPanel(userProcessContainer, carDir);
        userProcessContainer.add("SearchJPanel",panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_btnSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnfleetCatalog;
    // End of variables declaration//GEN-END:variables

}
